# blake3 renamer

> A small script for renaming all supplied files to their blake3 hashes

## Purpose

This is my third iteration on the idea of renaming all the files in a directory
to some kind of hash of their contents. The main use is to run this on a
directory of pictures, as I organize them with metadata tags, so I don't need
their names, and just want to make sure there are no duplicates.

## Dependencies

Requires the [b3sum](https://crates.io/crates/b3sum) CLI utility for actually
calculating the blake3 sum, as well as the fish shell.

## Use

Accepts a variable amount of arguments, as well as a glob expansion.

```fish
fish blake3_renamer file1 file2 file3

fish blake3_renamer *.jpg
```
