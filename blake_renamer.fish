for file in $argv
    set root_name (echo $file | sed 's/\.[^.]*$//')
    set sum_name (b3sum --no-names $file)
    set extension (echo $file | sed 's/.*\.//')

    if test $root_name != $sum_name
        set -l new_name $sum_name.$extension
        echo $file "->" $new_name
        mv $file $new_name
    end
end
